#define END "DONE\n"

typedef struct
{
    char word[80];
} tree;

// menu
void menu()
{
    printf("a) add words to the tree\n");
    printf("p) print the person\n");
    printf("f) find a word in the tree\n");
    printf("q) quit\n");
    printf("Choice: ");

}

// get input
char get_input()
{
    char input[100];
    fgets(input, 100, stdin);
    return input[0];
}

// swap function
void swap(tree **arr, int x, int y)
{
  tree *temp = arr[x];
  arr[x] = arr[y];
  arr[y] = temp;
}

// sort input
void sort(tree **arr, int len)
{
  for(int i = 0; i < len - 1; i++)
  {
    for(int j = 0; j < len - 1; j++)
    {
       if(strcmp(arr[j]->word, arr[j+1]->word) > 0)
       {
         swap(arr, j, j+1);
       }
    }
  }
}