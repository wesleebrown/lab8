tree:  tree.o
	c99 tree.o -o tree -Wall
tree.o: tree.c
	c99 -c tree.c -g -Wall
clean:
	rm -f *.o tree
