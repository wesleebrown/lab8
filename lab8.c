// includes
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "tree.h"

// function prototypes
char get_input();
void sort(tree **arr, int len);
void menu();

int main()
{
    int size = 0;
    tree *arr[1000];
    while (1)
    {
        menu();
        char in = get_input();

        switch(in)
        {
            case 'a':
                arr[size] = (tree *)malloc(sizeof(tree));
                printf("Enter word(s), type 'DONE' when finished\n> ");
                fgets(arr[size]->word, 80, stdin);
		while(strcmp(arr[size]->word, END) != 0)
		{
		  size++;
		  arr[size] = (tree *)malloc(sizeof(tree));
		  printf("> ");
		  fgets(arr[size]->word, 80, stdin);
		}
                break;
		
            case 'p':
		if(arr[size]->word == NULL)
                {
	          printf("The word tree is empty. Use option a to add words to the tree.\n");
	          break;
	        }	
		printf("Current Tree: %d items\n", size);
               for (int i = 0; i < size; i++)
               {
		 sort(arr,size);
                 printf("%d)     %s", i+1, arr[i]->word);
               }
               break;
		
	    case 'f':
              if(arr[size]->word == NULL)
              {
	        printf("The word tree is empty. Use option a to add words to the tree.\n");
		break;
	      }	
              else
	      {
		char search[80];
		int i = 0;
	        printf("Search for a word > ");
	        fgets(search, 80, stdin);
		while(strcmp(arr[i]->word, search) != 0)
		{
		  printf("searching for %s ...%s", search, arr[i]->word);
		  if(i == size)
		  {
		    printf("CANT FIND %s", search);
		    break;
		  }
		  i++;
		}
		{
		  printf("FOUND: item number: %d     %s", i+1, arr[i]->word);
		}
	      }
	      break;
	      
	    case 'q':
                exit(0);
                break;
		
            default:   
                printf("Use a, p, f or q\n");
                break;
        }
    }
    return 0;
}